import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { DataService } from '../data.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  private addNickname: String;
  private addPassword: String;
  private addFirstname: String;
  private addLastname: String;
  private addEmail: String;
  private addTeachNickname: String;
  private addTeachPassword: String;
  private addTeachFirstname: String;
  private addTeachLastname: String;
  private addTeachEmail: String;
  private addSection: String;
  private sections: any;
  private teacherInSection: boolean;
  private tempId: any;
  private tempSection: any;

  constructor(private http: HttpClient, private dataService: DataService) { }

  ngOnInit() {
    this.dataService.get_sections().subscribe((res: any) => {
      this.sections = res;
    });
  }

  //async getSectionProjects(idteacher: String) {
  //  return await this.dataService.get_section_projects(idteacher);
  //}
  
  async getIdTeacher(section: String) {
    return await this.dataService.get_id_teacher(section);
  }

  newSection() {
    this.teacherInSection = false;
  }

  newTeacher() {
    this.teacherInSection = true;
  }

  onFormSubmit(userForm: NgForm) {
    if (userForm.value.teacher_nickname && userForm.value.teacher_password && userForm.value.teacher_firstname && userForm.value.teacher_lastname && userForm.value.teacher_email && userForm.value.teacher_section) {
      this.http.post('http://localhost:3000/new/user', {
        nickname: userForm.value.teacher_nickname,
        password: userForm.value.teacher_password,
        firstname: userForm.value.teacher_firstname,
        lastname: userForm.value.teacher_lastname,
        email: userForm.value.teacher_email,
        section: userForm.value.teacher_section,
        status: true
      })
        .subscribe(
          res => {
            this.addNickname = "";
            this.addPassword = "";
            this.addFirstname = "";
            this.addLastname = "";
            this.addEmail = "";
            this.addSection = "";
          },
          err => {
            console.log("Error occured");
          }
        )
    }
  }

  async onTeacherFormSubmit(newTeacherForm: NgForm) {
    if (newTeacherForm.value.teach_nickname && newTeacherForm.value.teach_password && newTeacherForm.value.teach_firstname && newTeacherForm.value.teach_lastname && newTeacherForm.value.teach_email && newTeacherForm.value.teach_section) {
      //this.tempId = this.getIdTeacher(newTeacherForm.value.teach_section);
      //this.tempSection = await this.getSectionProjects(this.tempId);
      //console.log(this.tempSection);
      this.http.post('http://localhost:3000/new/user', {
        nickname: newTeacherForm.value.teach_nickname,
        password: newTeacherForm.value.teach_password,
        firstname: newTeacherForm.value.teach_firstname,
        lastname: newTeacherForm.value.teach_lastname,
        email: newTeacherForm.value.teach_email,
        section: newTeacherForm.value.teach_section,
        status: true
      })
        .subscribe(
          res => {
            this.addTeachNickname = "";
            this.addTeachPassword = "";
            this.addTeachFirstname = "";
            this.addTeachLastname = "";
            this.addTeachEmail = "";
          },
          err => {
            console.log("Error occured");
          }
        )
    }
  }

}
