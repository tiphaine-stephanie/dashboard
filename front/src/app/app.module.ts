import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from "@angular/common/http";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { UserMaintableComponent } from './user-maintable/user-maintable.component';
import { AdminUserstableComponent } from './admin-userstable/admin-userstable.component';
import { UserChartComponent } from './user-chart/user-chart.component';
import { FormsModule } from '@angular/forms';

import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin/admin.component';

const appRoutes: Routes = [
  { 
    path: 'forTheRegistrationComponent',
    component: RegistrationComponent
  },
  {
    path: 'forTheLoginComponent',
    component: LoginComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegistrationComponent,
    UserMaintableComponent,
    AdminUserstableComponent,
    UserChartComponent,
    AdminComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

