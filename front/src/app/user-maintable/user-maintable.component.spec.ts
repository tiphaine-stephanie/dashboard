import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserMaintableComponent } from './user-maintable.component';

describe('UserMaintableComponent', () => {
  let component: UserMaintableComponent;
  let fixture: ComponentFixture<UserMaintableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserMaintableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserMaintableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
